#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/mutex.h>
#include <linux/kfifo.h>
#include <linux/fs.h>
#include <asm/uaccess.h>

#define SUCCESS 0
#define ERROR -1

#define DEVICE_NAME "device"
#define CLASS_NAME "devClass"
#define MSG_FIFO_SIZE 1024
#define MSG_FIFO_MAX  128

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexander Solomin");
MODULE_DESCRIPTION("A simple Linux sysfs repeating driver");
MODULE_VERSION("0.5");

static bool debug = false;
module_param(debug, bool, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(debug, "enable debug info (default: false)");

// using this macro's for output more handy
#define dbg(format, arg...) do { if (debug) pr_info(CLASS_NAME ": %s: " format, __FUNCTION__, ## arg); } while (0)
#define err(format, arg...) pr_err(CLASS_NAME ": " format, ## arg)
#define info(format, arg...) pr_info(CLASS_NAME ": " format, ## arg)
#define warn(format, arg...) pr_warn(CLASS_NAME ": " format, ## arg)

static struct class* class = NULL;
static struct device* device = NULL;
static int major;

static bool message_read;
static DEFINE_MUTEX(device_mutex);
static DECLARE_KFIFO(msg_fifo, char, MSG_FIFO_SIZE);
static unsigned int msg_len[MSG_FIFO_MAX];
static int msg_idx_rd, msg_idx_wr;

//functions
static int device_open(struct inode* inode, struct file* filp);
static int device_close(struct inode* inode, struct file* filp);
static ssize_t device_read(struct file* filp, char __user *buffer, size_t length, loff_t* offset);
static ssize_t sys_add_to_fifo(struct device* dev, struct device_attribute* attr, const char* buf, size_t count);
static ssize_t sys_reset(struct device* dev, struct device_attribute* attr, const char* buf, size_t count);

static DEVICE_ATTR(fifo, S_IWUSR, NULL, sys_add_to_fifo);
static DEVICE_ATTR(reset, S_IWUSR, NULL, sys_reset);

static struct file_operations fops = {
	.read = device_read,
	.open = device_open,
	.release = device_close
};

int init_module(void)
{
	dbg("");

	major = register_chrdev(0, DEVICE_NAME, &fops);
	if (major < 0)
	{
		err("failed to register device: error %d\n", major);
		return ERROR;
	}

	class = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(class))
	{
		err("failed to register device class '%s'\n", CLASS_NAME);
		unregister_chrdev(major, DEVICE_NAME);
		return ERROR;
	}

	device = device_create(class, NULL, MKDEV(major, 0), NULL, CLASS_NAME "_" DEVICE_NAME);
	if (IS_ERR(device))
	{
		err("failed to create device '%s_%s'\n", CLASS_NAME, DEVICE_NAME);
		class_destroy(class);
		unregister_chrdev(major, DEVICE_NAME);
		return ERROR;
	}

	if (device_create_file(device, &dev_attr_fifo) < 0)
	{
		warn("failed to create write /sys endpoint - continuing without\n");
	}

	mutex_init(&device_mutex);
	INIT_KFIFO(msg_fifo);
	msg_idx_rd = msg_idx_wr = 0;

	return SUCCESS;
}


void cleanup_module (void)
{
	dbg("");
	device_remove_file(device, &dev_attr_fifo);
	device_remove_file(device, &dev_attr_reset);
	device_destroy(class, MKDEV(major, 0));
	class_destroy(class);
	unregister_chrdev(major, DEVICE_NAME);
}


static int device_open(struct inode* inode, struct file* filp)
{
	dbg("");

/*does not allow write access */
	if ( ((filp->f_flags & O_ACCMODE) == O_WRONLY) || ((filp->f_flags & O_ACCMODE) == O_RDWR) )
	{
		warn("write access is prohibited\n");
		return -EACCES;
	}

	if (!mutex_trylock(&device_mutex))
	{
		warn("another process is accessing the device\n");
		return -EBUSY;
	}

	message_read = false;
	return 0;
}

static int device_close(struct inode* inode, struct file* filp)
{
	dbg("");
	mutex_unlock(&device_mutex);
	return 0;
}

static ssize_t device_read(struct file* filp, char __user *buffer, size_t length, loff_t* offset)
{
	unsigned int copied;

	if (message_read) return 0;
	dbg("");

	if (kfifo_is_empty(&msg_fifo))
	{
		dbg("no message in fifo\n");
		return 0;
	}

	kfifo_to_user(&msg_fifo, buffer, msg_len[msg_idx_rd], &copied);

	if (msg_len[msg_idx_rd] != copied)
	{
		warn("short read detected\n");
	}

	msg_idx_rd = (msg_idx_rd+1)%MSG_FIFO_MAX;
	message_read = true;

	return copied;
}

static ssize_t sys_add_to_fifo(struct device* dev, struct device_attribute* attr, const char* buf, size_t count)
{
	unsigned int copied;

	dbg("");
	if (kfifo_avail(&msg_fifo) < count)
	{
		warn("not enough space in fifo\n");
		return -ENOSPC;
	}

	if ((msg_idx_wr+1)%MSG_FIFO_MAX == msg_idx_rd)
	{
		warn("message length table is full\n");
		return -ENOSPC;
	}

	copied = kfifo_in(&msg_fifo, buf, count);
	msg_len[msg_idx_wr] = copied;
	if (copied != count)
	{
		warn("short write detected\n");
	}
	msg_idx_wr = (msg_idx_wr+1)%MSG_FIFO_MAX;

 return copied;
}

static ssize_t sys_reset(struct device* dev, struct device_attribute* attr, const char* buf, size_t count)
{
	dbg("");

	kfifo_reset(&msg_fifo);
	msg_idx_rd = msg_idx_wr = 0;

	return count;
}
