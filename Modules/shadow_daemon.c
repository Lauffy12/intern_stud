#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#define SUCCESS 0
#define FAIL -1
#define DEFAULT_FILE "/etc/shadow"
#define FILE_NAME_SIZE 256			//just in case

char fileName[FILE_NAME_SIZE];

void signal_handler(int signum);
int read_file (char * path);
void printHelp (char *softName);

int main(int argc, char* argv[])
{
	strcpy(fileName, DEFAULT_FILE);

	char opt;

	while ((opt = getopt(argc, argv, "f:h")) != -1)     //checking argumants
	{
	  switch (opt)
	  {
		  case 'f': strcpy(fileName,optarg);
		  break;

		  case 'h' : { printHelp(argv[0]); return 0;}
	  }
	}

	while(1)
	{
		sleep(1);
		signal(SIGUSR1, signal_handler);
	}
	return 0;
}


void signal_handler(int signum)
{
	if (signum == SIGUSR1)
	{
		printf("Trying to open file :%s \n", fileName);
		if (read_file(fileName) != SUCCESS)
		{
			perror("Can't read file");
		}
	}
}

int read_file (char * path)
{
	FILE *f;
	char c;

	f = fopen(path, "r");
	if (NULL == f)
	{
		perror("FOPEN ERROR. Can't open file.");
		return FAIL;
	}

	while((c=fgetc(f))!=EOF)
	{
		printf("%c",c);
	}
	fclose(f);
	return SUCCESS;
}

void printHelp (char *softName)
{
	printf("File reader \n Usage: %s -f [file name]\nRuning without parameters will read default file: %s\n" ,softName,DEFAULT_FILE);
}
