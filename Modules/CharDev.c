#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
/*
 *  Прототипы функций, обычно их выносят в заголовочный файл (.h)
 */
int init_module (void);
void cleanup_module (void);
static int device_open (struct inode *, struct file *);
static int device_release (struct inode *, struct file *);
static ssize_t device_read (struct file *, char *, size_t, loff_t *);
static ssize_t device_write (struct file *, const char *, size_t, loff_t *);

#define SUCCESS 0
#define ERROR -1
#define DEVICE_NAME "charDevice"
#define CLASS_NAME "charClass"
#define MODULE_NAME "CharDev"
#define BUF_LEN 256

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexander Solomin");
MODULE_DESCRIPTION("A simple Linux char driver");
MODULE_VERSION("0.1");

static int major;
static int deviceOpen = 0;
static int openCount = 0;
static char msg[BUF_LEN] = {0};
static short sizeOfMessage;
static struct class*  devClass  = NULL;
static struct device* charDevice = NULL;

static struct file_operations fops = {
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release
};


int init_module (void)
{
	major = register_chrdev (0, DEVICE_NAME, &fops);

	if (major < 0)
	{
		printk (KERN_ALERT "%s: Registering the character device failed with %d\n", MODULE_NAME, major);
		return ERROR;
	}

	devClass = class_create(THIS_MODULE,CLASS_NAME);
	if (IS_ERR(devClass))
	{
		unregister_chrdev(major,DEVICE_NAME);
		printk(KERN_ALERT "%s: Failed to register device class", MODULE_NAME);
		return PTR_ERR(devClass);          // Correct way to return an error on a pointer
	}
	printk(KERN_INFO "%s: device class registered correctly. Major number: %d\n", MODULE_NAME, major);

	charDevice = device_create(devClass, NULL, MKDEV(major, 0), NULL, DEVICE_NAME);
	if (IS_ERR(charDevice))
	{
		class_destroy(devClass);
		unregister_chrdev(major, DEVICE_NAME);
		printk(KERN_ALERT "%s: Failed to create device class", MODULE_NAME);
		return PTR_ERR(charDevice);
	}
	printk(KERN_INFO "%s: device class created correctly\n", MODULE_NAME);

	return 0;
}

void cleanup_module (void)
{
	device_destroy(devClass, MKDEV(major, 0));
	class_unregister(devClass);
	class_destroy(devClass);
	unregister_chrdev (major, DEVICE_NAME);
	printk(KERN_INFO "%s: Cleanup complete. Have a nice day!", MODULE_NAME);
}


static int device_open (struct inode *inode, struct file *file)
{
	openCount++;
	if (deviceOpen)
	{
		return -EBUSY;
	}

	deviceOpen++;
	printk (KERN_INFO "%s Device opend %d times.",MODULE_NAME, openCount);

	return SUCCESS;
}


static int device_release (struct inode *inode, struct file *file)
{
  deviceOpen--;
  printk(KERN_INFO "%s Device has been closed", MODULE_NAME);
  return 0;
}

/* calling when already opend */
static ssize_t device_read (struct file *filp, char *buffer, size_t length, loff_t * offset)
{
	int error_count = 0;
	// copy_to_user has the format ( * to, *from, size) and returns 0 on success
	error_count = copy_to_user(buffer, msg, sizeOfMessage);

	if (error_count!=0)
	{
		printk(KERN_INFO "%s: Failed to send %d characters to the user\n",MODULE_NAME, error_count);
		return -EFAULT;
	}

	printk(KERN_INFO "%s: Sent %d characters to the user\n",MODULE_NAME, sizeOfMessage);
	sizeOfMessage = 0;
	return SUCCESS;
}

/* calling when proc trying to write in dev/mydev */
static ssize_t device_write (struct file *filp, const char *buffer, size_t len, loff_t * off)
{
	sprintf(msg, "%s(%lu letters)", buffer, len);   // appending received string with its length
	sizeOfMessage = strlen(msg);                 // store the length of the stored message
	printk(KERN_INFO "%s: Recived %lu characters from the user\n",MODULE_NAME, len);
	return len;
}
