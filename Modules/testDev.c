//#requiers kernel module file nearby

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#define BUFFER_LENGTH 256
#define DEVICE_NAME "/dev/charDevice"
#define EXIT_CMD "exit"


static char receive[BUFFER_LENGTH];     ///< The receive buffer from the LKM

int main()
{

	system("insmod CharDev.ko");

	int fd;
	char stringToSend[BUFFER_LENGTH];

	fd = open(DEVICE_NAME, O_RDWR);             // Open the device with read/write access
	if (fd < 0)
	{
		perror("Failed to open the device...");
		return errno;
	}

	while (1)
	{
		printf("String to kernel module:\n");
		scanf("%[^\n]%*c", stringToSend);                // Read in a string (with spaces)


		if(write(fd, stringToSend, strlen(stringToSend)) < 0) // Send the string to the LKM
		{
			perror("Failed to write the message to the device.");
			return errno;
		}

		if(read(fd, receive, BUFFER_LENGTH) < 0)        // Read the response from the LKM
		{
			perror("Failed to read the message from the device.");
			return errno;
		}
		printf("MEssage from kernel module: [%s]\n", receive);

		if (strcmp(stringToSend, EXIT_CMD) == 0)
		{
			close(fd);
			system("rmmod CharDev");
			return 0;
		}
	}
	return 0;
}
