#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/skbuff.h>
#include <linux/udp.h>
#include <linux/ip.h>

#define SUCCESS 0
#define ERROR -1
#define MODULE_NAME "NETFILTER MODULE"


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexander Solomin");
MODULE_DESCRIPTION("Netfilter for educational purpose");
MODULE_VERSION("0.1");

/* This function to be called by hook. */
static unsigned int
hook_func(unsigned int hooknum,
        struct sk_buff *skb,
        const struct net_device *in,
        const struct net_device *out,
        int (*okfn) (struct sk_buff *))
{
    struct udphdr *udp_header;
    struct iphdr *ip_header = (struct iphdr *)skb_network_header(skb);

    if (ip_header->protocol == 17) {
        udp_header = (struct udphdr *)skb_transport_header(skb);
		if (udp_header->dest == htons(8698))
		{
			printk(KERN_INFO "[%s]got UDP pack to %d.\n",MODULE_NAME,
			ntohs(udp_header->dest));

			udp_header->dest = htons(8697);
			printk(KERN_INFO "[%s]rerouting pack to %d.\n",MODULE_NAME,
			ntohs(udp_header->dest));
		}
    }

    return NF_ACCEPT;
}

static struct nf_hook_ops nfho = {
    .hook       = hook_func,
    .hooknum    = NF_INET_PRE_ROUTING,
    .pf         = PF_INET,
    .priority   = NF_IP_PRI_FIRST,
};

int init_module(void)
{
    printk(KERN_INFO "[%s]Register netfilter module.\n",MODULE_NAME);
    nf_register_hook(&nfho);

    return SUCCESS;
}

void cleanup_module(void)
{
    printk(KERN_INFO "[%s]Unregister netfilter module.\n", MODULE_NAME);
    nf_unregister_hook(&nfho);
}
