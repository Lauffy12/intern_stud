#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ioctl.h>
#include <linux/fs.h>
#include <asm/uaccess.h>

#define SUCCESS 0
#define MODULE_NAME "ioctlDev"
#define DEVICE_NAME "ioctlDevice"
#define BUF_LEN 256
#define DEBUG

#define MAJOR_NUM 100
#define IOCTL_SET_MSG _IOR(MAJOR_NUM, 0, char *)
#define IOCTL_GET_MSG _IOR(MAJOR_NUM, 1, char *)
#define IOCTL_GET_NTH_BYTE _IOWR(MAJOR_NUM, 2, int)

static char *message_ptr;
static int openCount = 0;
static char message[BUF_LEN];

static int device_open (struct inode *inode, struct file *file);
static int device_release (struct inode *inode, struct file *file);
static ssize_t device_read (struct file *file, char __user * buffer, size_t length, loff_t * offset);
static ssize_t device_write (struct file *file, const char __user * buffer, size_t length, loff_t * offset);
long device_ioctl (struct file *file, unsigned int ioctl_num, unsigned long ioctl_param);

struct file_operations fops = {
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release,
	.unlocked_ioctl = device_ioctl,
};

int init_module ()
{
	int ret_val;
	ret_val = register_chrdev (MAJOR_NUM, DEVICE_NAME, &fops);
	if (ret_val < 0)
	{
		printk(KERN_ALERT "%s: failed with %d\nSorry, registering the character device ",MODULE_NAME, ret_val);
		return ret_val;
	}

	printk (KERN_INFO "%s: The major device number is %d.\nRegisteration is a succed",MODULE_NAME, MAJOR_NUM);

	return 0;
}

void cleanup_module ()
{
  unregister_chrdev (MAJOR_NUM, DEVICE_NAME);
  printk (KERN_INFO "%s Cleanup is a success",MODULE_NAME);
}

static int device_open (struct inode *inode, struct file *file)
{
  printk (KERN_INFO "%s: device_open(%p)\n",MODULE_NAME, file);
  if (openCount)
  {
    return -EBUSY;
  }
  openCount++;
  message_ptr = message;
  try_module_get(THIS_MODULE);
  return SUCCESS;
}

static int device_release (struct inode *inode, struct file *file)
{
  printk (KERN_INFO "%s: device_release(%p,%p)\n",MODULE_NAME, inode, file);
  openCount--;
  module_put (THIS_MODULE);
  return SUCCESS;
}

/* calling when somethig trying to read from dev file */
static ssize_t device_read (struct file *file, char __user * buffer, size_t length, loff_t * offset)
{
  int bytes_read = 0;

  printk (KERN_INFO "%s: device_read(%p,%p,%lu)\n",MODULE_NAME, file, buffer, length);

  if (*message_ptr == 0)
    return 0;

  while (length && *message_ptr)
  {
    put_user (*(message_ptr++), buffer++);
    length--;
    bytes_read++;
  }

  printk (KERN_INFO "%s: Read %d bytes, %lu left\n",MODULE_NAME, bytes_read, length);

  return bytes_read;
}

/* calling when trying to write in dev file */
static ssize_t device_write (struct file *file, const char __user * buffer, size_t length, loff_t * offset)
{
  int i;

  printk (KERN_INFO "%s: device_wrie(%p,%p,%lu)\n",MODULE_NAME, file, buffer, length);

  for (i = 0; i < length && i < BUF_LEN; i++)
  {
    get_user(message[i], buffer + i);
  }
  message_ptr = message;
  return i;
}

/* inout control func */
long device_ioctl (struct file *file, unsigned int ioctl_num, unsigned long ioctl_param)
{
  int i;
  char *temp;
  char ch;

/* ioctl reactions */
  switch (ioctl_num) {
  case IOCTL_SET_MSG:

    temp = (char *)ioctl_param;
    get_user (ch, temp);
    for (i = 0; ch && i < BUF_LEN; i++, temp++)
    {
      get_user(ch, temp);
    }
    device_write (file, (char *)ioctl_param, i, 0);
    break;

  case IOCTL_GET_MSG:
    i = device_read (file, (char *)ioctl_param, 99, 0);
    put_user('\0', (char *)ioctl_param + i);
    break;

  case IOCTL_GET_NTH_BYTE:
    return message[ioctl_param];
    break;
  }

  return SUCCESS;
}
