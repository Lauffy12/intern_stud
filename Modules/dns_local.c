#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/skbuff.h>
#include <linux/udp.h>
#include <linux/tcp.h>
#include <linux/ip.h>
#include <net/ip.h>

#define SUCCESS 0
#define ERROR -1
#define MODULE_NAME "DNS_PROXY_MODULE"

#define DNS_SERVER_PORT   53
#define UPD_HDRLEN         8
#define DNS_HDRLEN        12
#define DNS_ANSWLEN       16
#define DNS_Q_CONSTLEN     4
#define DNS_A_OFFSET       2
#define QNAME_MAXLEN      63
#define CONSTMAGIC         1

#define FLAG_FILLING_0     0
#define FLAG_FILLING_1     1
#define FLAG_FILLING_2     2

#define HASHTAB_SIZE    8192

static unsigned short DNSH_ID;
static unsigned int   DNSA_ANCOUNT;


struct DNS_HEADER
{
	unsigned short id;
	unsigned char  rd :1;
	unsigned char  tc :1;
	unsigned char  aa :1;
	unsigned char  opcode :4;
	unsigned char  qr :1;  
	unsigned char  rcode :4;
	unsigned char  cd :1;
	unsigned char  ad :1;
	unsigned char  z :1;
	unsigned char  ra :1;
	  
	unsigned short q_count;
	unsigned short ans_count;
	unsigned short auth_count;
	unsigned short add_count;
};

struct _QUESTION
{
	unsigned short qtype;
	unsigned short qclass;
};

struct QUESTION
{
	unsigned char    *q_name;
	struct _QUESTION *question;
};
 
struct R_DATA
{
	unsigned short type;
	unsigned short _class;
	unsigned int   ttl;
	unsigned short data_len;
};

struct ANSWER
{
	unsigned char *name;
	struct R_DATA *answer;
	unsigned char *rdata;
};

struct NODE
{
	unsigned char *key;
	void          *value;
	unsigned int   key_len;
	unsigned int   value_len;

	struct NODE   *next;
};

struct DATA_NODE
{
	unsigned int  flag;
	struct NODE  *node;
};

static struct NODE *hashtab[HASHTAB_SIZE];

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexander Solomin");
MODULE_DESCRIPTION("DNS proxy using netfilter");
MODULE_VERSION("0.1");

static unsigned int get_datalen_question(struct QUESTION *dnsq)
{
	unsigned char *buff;
	unsigned int   i;
	unsigned int   index = 0;
	unsigned int   bufflen = 1;

	buff = (unsigned char *)kmalloc(bufflen, GFP_KERNEL);
	memset(buff, 0, bufflen);
	memcpy(buff, dnsq, bufflen);

	for ( ; (unsigned int)buff[index] != 0; ) 
	{
		for (i = 1; i <= (QNAME_MAXLEN + 1); i++)
		{
			if ((unsigned int)buff[index] == i)
			{
				bufflen += i;
				bufflen++;
				index = bufflen;
				index--;
				break;
			}
		}

		if (i == (QNAME_MAXLEN + 1))
		{
			printk(KERN_ERR "\n[%s] ERROR: doamin name is larger then '%d' symbols\n\n",
			       MODULE_NAME, QNAME_MAXLEN+1);
			kfree(buff);
			return -1;
		}

		buff = (unsigned char *)kmalloc(bufflen, GFP_KERNEL);
		memset(buff, 0, bufflen);
		memcpy(buff, dnsq, bufflen);
	}

	if ( (index == 0) && ((unsigned int)buff[index] == 0) )
	{
		printk(KERN_ERR "\n[%s] ERROR: length domain name is equal '0' symbols\n\n",
		       MODULE_NAME);
		kfree(buff);
		return -1;
	}

	kfree(buff);
	return bufflen;
}

static unsigned int get_datalen_answer(void)
{
	return (DNS_ANSWLEN * (DNSA_ANCOUNT + CONSTMAGIC));
}

static void send_reply_dnspacket(
	struct sk_buff *in_skb,
	unsigned int dst_ip,
	unsigned int dst_port,
	unsigned int src_ip,
	struct NODE *node)
{
	struct sk_buff       *nskb;
	struct iphdr         *ip_h;
	struct udphdr        *udp_h;

	struct DNS_HEADER    *dns_h;
	struct _QUESTION     *dns__q;

	unsigned char        *data_q;
	void                 *data_a;

	unsigned int         dns_q_len;
	unsigned int         dns_a_len;
	unsigned int         udp_len;

	dns_q_len = (node->key_len);
	dns_a_len = (node->value_len);
	udp_len   = (UPD_HDRLEN + DNS_HDRLEN + dns_q_len + dns_a_len);

	nskb = alloc_skb(sizeof(struct iphdr) + udp_len + LL_MAX_HEADER, GFP_ATOMIC);
	if (!nskb)
	{
		printk (KERN_ERR "[%s] ERROR! Allocate memory to DNS reply\n", MODULE_NAME);
		return;
	}

	skb_reserve(nskb, LL_MAX_HEADER);
	skb_reset_network_header(nskb);

	ip_h = (struct iphdr *)skb_put(nskb, sizeof(struct iphdr));

	ip_h->version  = 4;
	ip_h->ihl      = sizeof(struct iphdr) / 4;
	ip_h->ttl      = 64;
	ip_h->tos      = 0;
	ip_h->id       = 0;
	ip_h->frag_off = htons(IP_DF);
	ip_h->protocol = IPPROTO_UDP;
	ip_h->saddr    = src_ip;
	ip_h->daddr    = dst_ip;
	ip_h->tot_len  = htons(sizeof(struct iphdr) + udp_len);
	ip_h->check    = 0;
	ip_h->check    = ip_fast_csum((unsigned char *)ip_h, ip_h->ihl);

	udp_h = (struct udphdr *)skb_put(nskb, UPD_HDRLEN);
	memset(udp_h, 0, sizeof(*udp_h));

	udp_h->source = htons(DNS_SERVER_PORT);
	udp_h->dest   = dst_port;
	udp_h->len    = htons(udp_len);

	dns_h = (struct DNS_HEADER *)skb_put(nskb, DNS_HDRLEN);
	dns_h->id         = (unsigned short) htons(DNSH_ID);
	dns_h->qr         = 0;
	dns_h->opcode     = 0;
	dns_h->aa         = 0;
	dns_h->tc         = 0;
	dns_h->rd         = 1;
	dns_h->ra         = 0;
	dns_h->z          = 0;
	dns_h->ad         = 0;
	dns_h->cd         = 0;
	dns_h->rcode      = 0;
	dns_h->q_count    = htons(1);
	dns_h->ans_count  = htons(DNSA_ANCOUNT);
	dns_h->auth_count = 0;
	dns_h->add_count  = 0;

	skb_dst_set(nskb, dst_clone(skb_dst(in_skb)));
	nskb->protocol = htons(ETH_P_IP);

	data_q = (char *)skb_put(nskb, dns_q_len);
	memcpy(data_q, (node->key), dns_q_len);

	dns__q = (struct _QUESTION *)skb_put(nskb, DNS_Q_CONSTLEN);
	dns__q->qtype  = htons(1);
	dns__q->qclass = htons(1);

	data_a = (void *)skb_put(nskb, dns_a_len);
	memcpy(data_a, (node->value), dns_a_len);

	udp_h->check  = 0;
	udp_h->check  = csum_tcpudp_magic(src_ip, dst_ip,
	                                  udp_len, IPPROTO_UDP,
	                                  csum_partial(udp_h, udp_len, 0));

	struct net *nets;

	if (ip_route_me_harder(nets, nskb, RTN_UNSPEC))
	{
		printk (KERN_ERR "\n[%s] ERROR: fail function ip_route_me_harder()\n",
		        MODULE_NAME);
		kfree_skb(nskb);
	}

	ip_local_out(nets, nskb->sk, nskb);
	return;
}

static unsigned int rs_hash(unsigned char *str, unsigned int len)
{
	unsigned int b = 378551;
	unsigned int a = 63689;
	unsigned int hash = 0;
	unsigned int i = 0;

	for (i = 0; i < len; str++, i++)
	{
		hash = hash * a + (unsigned char)(*str);
		a *= b;
	}
	return (hash % HASHTAB_SIZE);
}

static void hashtab_init(struct NODE **hashtab)
{
	unsigned int i;

	for (i = 0; i < HASHTAB_SIZE; i++) 
	{
		hashtab[i] = NULL;
	}
}

static struct DATA_NODE hashtab_lookup(
	struct NODE    **hashtab,
	struct QUESTION *dnsq,
	unsigned int     dnsqlen)
{
	unsigned char *key;
	unsigned int   index = 0;

	struct NODE   *node;

	struct DATA_NODE dnode = {
		.flag = FLAG_FILLING_0,
		.node = NULL,
	};

	key = (unsigned char *)kmalloc(dnsqlen, GFP_KERNEL);
	memset(key, 0, dnsqlen);
	memcpy(key, dnsq, dnsqlen);

	index = rs_hash(key, dnsqlen);

	for (node = hashtab[index];	node != NULL; node = node->next)
	{
		if (memcmp(node->key, key, dnsqlen) == 0)
		{
			dnode.flag = FLAG_FILLING_1;
			dnode.node = node;
			if (node->value_len != 0) dnode.flag = FLAG_FILLING_2;
		}
	}

	kfree(key);
	return dnode;
}

static void hashtab_add_key(
	struct NODE **hashtab,
	struct QUESTION *dnsq,
	unsigned int dnsqlen)
{
	unsigned char *key;
	unsigned int   index = 0;

	struct NODE   *node;

	key = (unsigned char *)kmalloc(dnsqlen, GFP_KERNEL);
	memset(key, 0, dnsqlen);
	memcpy(key, dnsq, dnsqlen);

	index = rs_hash(key, dnsqlen);

	node = kmalloc(sizeof(*node), GFP_KERNEL);
	if (node != NULL)
	{
		node->key       = key;
		node->key_len   = dnsqlen;
		node->value_len = 0;
		node->next      = hashtab[index];
		hashtab[index]  = node;
	}
}

static void hashtab_add_value(
	struct NODE *node,
	struct ANSWER *dnsa,
	unsigned int dnsalen)
{
	void *value;

	value = (void *)kmalloc(dnsalen, GFP_KERNEL);
	memset(value, 0, dnsalen);
	memcpy(value, dnsa, dnsalen);

	node->value     = value;
	node->value_len = dnsalen;
}

static unsigned int hook_post(
	unsigned int hooknum,
	struct sk_buff *skb,
	const struct net_device *in,
	const struct net_device *out,
	int (*okfn)(struct sk_buff *))
{
	struct iphdr     *ip_h;
	struct udphdr    *udp_h;

	struct DNS_HEADER    *dns_h;
	struct QUESTION  *dns_q;

	struct DATA_NODE  dnode;

	unsigned int dns_q_qnamelen = 0;

	if (skb->protocol == htons(ETH_P_IP))
	{
		ip_h = ip_hdr(skb);

		if (ip_h->protocol == IPPROTO_UDP)
		{
			udp_h = (void *)(struct updhdr *)skb_transport_header(skb);

			if (udp_h->dest == ntohs(DNS_SERVER_PORT))
			{

				dns_h = (struct DNS_HEADER *)
				        (skb->data + (ip_h->ihl * 4) + UPD_HDRLEN);

				DNSH_ID = ntohs(dns_h->id);

				dns_q = (struct QUESTION *)
				        (skb->data + (ip_h->ihl * 4) + UPD_HDRLEN + DNS_HDRLEN);

				dns_q_qnamelen = get_datalen_question(dns_q);
				if (dns_q_qnamelen <= 0) return NF_ACCEPT;

				dnode = hashtab_lookup(hashtab, dns_q, dns_q_qnamelen);

				if (dnode.flag == FLAG_FILLING_0)
					hashtab_add_key(hashtab, dns_q, dns_q_qnamelen);

				else if ( (dnode.flag == FLAG_FILLING_2) && (dnode.node != NULL) )
				{
					send_reply_dnspacket(skb, ip_h->saddr, \
					                          udp_h->source, \
					                          ip_h->daddr, dnode.node);

					printk(KERN_ALERT "[%s] SEND REPLY\n", MODULE_NAME);

					return NF_DROP;
				}
			}
		}
	}
	return NF_ACCEPT;
}

static unsigned int hook_pre(
	unsigned int hooknum,
	struct sk_buff *skb,
	const struct net_device *in,
	const struct net_device *out,
	int (*okfn)(struct sk_buff *))
{
	struct iphdr        *ip_h;
	struct udphdr       *udp_h;

	struct DNS_HEADER   *dns_h;
	struct QUESTION     *dns_q;

	struct ANSWER       *dns_a;
	struct R_DATA       *dns__a;

	struct DATA_NODE     dnode;

	unsigned int dns_q_qnamelen = 0;
	unsigned int dns_a_len = 0;

	if (skb->protocol == htons(ETH_P_IP))
	{
		ip_h = ip_hdr(skb);

		if (ip_h->protocol == IPPROTO_UDP)
		{
			udp_h = (void *)(struct updhdr *)skb_transport_header(skb);

			if (udp_h->source == ntohs(DNS_SERVER_PORT))
			{
				dns_h = (struct DNS_HEADER *)
				        (skb->data + (ip_h->ihl * 4) + UPD_HDRLEN);

				DNSA_ANCOUNT = ntohs(dns_h->ans_count);

				dns_q = (struct QUESTION *)
				        (skb->data + (ip_h->ihl * 4) + UPD_HDRLEN + DNS_HDRLEN);

				dns_q_qnamelen = get_datalen_question(dns_q);
				if (dns_q_qnamelen <= 0) return NF_ACCEPT;

				dns__a = (struct R_DATA *)
				         (skb->data + (ip_h->ihl * 4) + UPD_HDRLEN + DNS_HDRLEN +
				         (dns_q_qnamelen + DNS_Q_CONSTLEN) + DNS_A_OFFSET);

				dns_a = (struct ANSWER *)
				        (skb->data + (ip_h->ihl * 4) + UPD_HDRLEN + DNS_HDRLEN +
				        (dns_q_qnamelen + DNS_Q_CONSTLEN));

				dns_a_len = get_datalen_answer();
				if (dns_a_len <= 0) return NF_ACCEPT;

				dnode = hashtab_lookup(hashtab, dns_q, dns_q_qnamelen);

				if ((dnode.flag == FLAG_FILLING_1) && (dnode.node != NULL))
					hashtab_add_value(dnode.node, dns_a, dns_a_len);
			}
		}
	}
	return NF_ACCEPT;
}

static struct nf_hook_ops HOOK_POST = {
	.hook     = (void *)hook_post,
	.pf       = NFPROTO_IPV4,
	.hooknum  = NF_INET_POST_ROUTING,
	.priority = NF_IP_PRI_FIRST,
};

static struct nf_hook_ops HOOK_PRE = {
	.hook     = (void *)hook_pre,
	.pf       = NFPROTO_IPV4,
	.hooknum  = NF_INET_PRE_ROUTING,
	.priority = NF_IP_PRI_FIRST,
};

int init_module(void)
{
	nf_register_hook(&HOOK_POST);
	nf_register_hook(&HOOK_PRE);

	hashtab_init(hashtab);


	printk(KERN_ALERT "[%s]Register netfilter module.\nHashtable size: %d\n",
	       MODULE_NAME,HASHTAB_SIZE);
	return SUCCESS;
}

void cleanup_module(void)
{
	nf_unregister_hook(&HOOK_POST);
	nf_unregister_hook(&HOOK_PRE);
	printk(KERN_INFO "[%s]Unregister netfilter module.\n", MODULE_NAME);
}
