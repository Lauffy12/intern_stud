#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MSG_SIZE 256
#define MAJOR_NUM 100
#define DEVICE_NAME "/dev/ioctlDevice"
#define IOCTL_SET_MSG _IOR(MAJOR_NUM, 0, char *)
#define IOCTL_GET_MSG _IOR(MAJOR_NUM, 1, char *)
#define IOCTL_GET_NTH_BYTE _IOWR(MAJOR_NUM, 2, int)

void ioctl_set_msg (int file_desc, char *message)
{
	int ret_val;

	ret_val = ioctl (file_desc, IOCTL_SET_MSG, message);

	if (ret_val < 0)
	{
		printf ("error on calling ioctl_set_msg: %d\n", ret_val);
		exit (-1);
	}
}

void ioctl_get_msg (int file_desc)
{
	int ret_val;
	char message[100];

	ret_val = ioctl (file_desc, IOCTL_GET_MSG, message);

	if (ret_val < 0)
	{
		printf ("error on calling ioctl_get_msg: %d\n", ret_val);
		exit (-1);
	}

	printf ("receved message (get_msg): %s\n", message);
}

void ioctl_get_nth_byte (int file_desc)
{
	int i;
	char c;

	printf ("Message returned: ");

	i = 0;
	while (c != 0)
	{
		c = ioctl (file_desc, IOCTL_GET_NTH_BYTE, i++);

		if (c < 0)
		{
			printf ("error on calling ioctl_get_nth_byte on %d byte.\n", i);
			exit (-1);
		}

		putchar (c);
	}
	putchar ('\n');
}

int main (int argc, char* argv[])
{
	system("insmod ioctlDev.ko");
	system("mknod /dev/ioctlDevice c 100 0");
	int file_desc;
	char *msg = "This message will be transmited to ioctlDev module";

	file_desc = open (DEVICE_NAME, 0);
	if (file_desc < 0)
	{
		printf ("Can't open device file: %s\n", DEVICE_NAME);
		exit (-1);
	}

	ioctl_set_msg (file_desc, msg);
	ioctl_get_msg (file_desc);
	ioctl_get_nth_byte (file_desc);

	close (file_desc);
	system("rmmod ioctlDev");
	system("rm /dev/ioctlDevice");
	return 0;
}
