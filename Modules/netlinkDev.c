#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/netlink.h>
#include <net/sock.h>
#include <net/net_namespace.h>

#define NETLINK_NITRO 17
#define SUCCESS 0
#define ERROR -1

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexander Solomin");
MODULE_DESCRIPTION("A simple Linux char driver");
MODULE_VERSION("0.1");

static void nl_data_ready (struct sk_buff *skb);
static void netlink_test(void);

static struct sock *nl_sk = NULL;
struct netlink_kernel_cfg cfg = {
	.input = nl_data_ready,
};

int init_module(void)
{
	printk(KERN_INFO "Initializing Netlink Socket");
	netlink_test();
	return 0;
}

void cleanup_module(void)
{
	printk(KERN_INFO "Goodbye");
	sock_release(nl_sk->sk_socket);
}

static void nl_data_ready (struct sk_buff *skb)
{

	struct nlmsghdr *nlh = NULL;
	if(skb == NULL)
	{
		printk("skb is NULL \n");
		return ;
	}
	nlh = (struct nlmsghdr *)skb->data;
	printk(KERN_INFO "%s: received netlink message payload: %s\n", __FUNCTION__, NLMSG_DATA(nlh));
}

static void netlink_test(void)
{
	nl_sk = netlink_kernel_create(&init_net, NETLINK_NITRO, &cfg);
}
