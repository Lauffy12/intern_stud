// Server source code for TCP-UDP/IP skills training

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/poll.h>


#define DEFAULT_TCP_PORT 8947
#define DEFAULT_UDP_PORT 8697
#define DEFAULT_IP "127.0.0.1"

#define LISTNER_ERROR -1
#define BIND_ERROR -2
#define UDP_INIT_ERROR -3
#define DEFAULT_ERROR -10

int listner, tcpSock, udpSock;
int bytsRecv;

int forkPid;

char msgBuf[256];

struct sockaddr_in tcpAddr;
struct sockaddr_in udpAddr;
struct sockaddr_in clientAddr;
struct pollfd readFds[2];        //file descriptors for poll;


void printHelp (char softName[]);

void sendResponse (int socket);

int main(int argc, char *argv[])
{

   int tcpPort = DEFAULT_TCP_PORT;
   int udpPort = DEFAULT_UDP_PORT;
   char ipAddr[] = DEFAULT_IP;

   system ("clear");

   char opt;

   while ((opt = getopt(argc, argv, "i:t:u:h")) != -1)     //checking argumants
   {
     switch (opt)
     {
        case 'i': strcpy(ipAddr,optarg);
        break;

        case 't': tcpPort = atoi(optarg);
        break;

        case 'u': udpPort = atoi(optarg);
        break;

        case 'h' : { printHelp(argv[0]); return 0;}
     }
  }

  listner = socket(AF_INET, SOCK_STREAM,0);              //creating listner socket
  if (listner < 0)
  {
     perror("Listner init error.");
     return LISTNER_ERROR;
  }

  udpSock = socket(AF_INET, SOCK_DGRAM,0);              //creating listner socket
  if (listner < 0)
  {
     perror("Udp socket init error.");
     return UDP_INIT_ERROR;
  }

  tcpAddr.sin_family = AF_INET;
  tcpAddr.sin_port = htons(tcpPort);
  tcpAddr.sin_addr.s_addr = inet_addr(ipAddr);

  udpAddr.sin_family = AF_INET;
  udpAddr.sin_port = htons(udpPort);
  udpAddr.sin_addr.s_addr = inet_addr(ipAddr);

  if (bind (listner,(struct sockaddr*)&tcpAddr,sizeof(tcpAddr)) < 0)  //binding socket
  {
     perror("Bind Tcp Error.");
     return BIND_ERROR;
  }

  if (bind(udpSock, (struct sockaddr*)&udpAddr, sizeof(udpAddr)) < 0)
  {
     perror("Bind Udp Error.");
     return BIND_ERROR;
  }

  unsigned int clientAddrSize = sizeof(struct sockaddr_in);

  readFds[0].fd = listner;
  readFds[0].events = POLLIN;
  readFds[1].fd = udpSock;
  readFds[1].events = POLLIN;

  listen(listner,100);       //listning for connections
  while (1)
  {
     if (poll(readFds, 2, 1000))
     {
        if (readFds[0].revents & POLLIN)
        {
           tcpSock = accept(listner, NULL, NULL);
           if (tcpSock < 0)
           {
              perror ("Accept Error.");
           }
           else
           {
             forkPid = fork();
             if (forkPid == 0)
             {
                close(listner);
                sendResponse(tcpSock);
                return 0;
             }
             else
             {
                close (tcpSock);
             }
           }
        }

        if (readFds[1].revents & POLLIN)
        {
           if (recvfrom(udpSock,msgBuf,sizeof(msgBuf),0,(struct sockaddr*)&clientAddr,&clientAddrSize) > 0)
          {
             forkPid = fork();
             if (forkPid == 0)
             {
                const time_t timer = time(NULL);
                strcpy(msgBuf,ctime(&timer));
                sendto(udpSock,&msgBuf,sizeof(msgBuf),0,(struct sockaddr*)&clientAddr,clientAddrSize);
                return 0;
             }
          }
        }
     }
  }

  return 0;
}

void sendResponse (int sock)
{
   const time_t timer = time(NULL);
   strcpy(msgBuf,ctime(&timer));
   sendto(sock,&msgBuf,sizeof(msgBuf),0,NULL,NULL);
   close (sock);
}

void printHelp (char *softName)
{
   printf("Simple TCP/IP server \n Usage: %s -t [server tcp port] -u [server udp port] - i [server ip] \nRuning without parameters will use default ip and port (%s:%i) for tcp/ip and (%s:%i) for udp connections\n" ,softName,DEFAULT_IP,DEFAULT_TCP_PORT,DEFAULT_IP,DEFAULT_UDP_PORT);
}
