#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define SOCKET_ERROR -1
#define BIND_ERROR -2

#define DEFAULT_IP "127.0.0.1"
#define DEFAULT_PORT 8697

int sock;
struct sockaddr_in servAddr;
struct sockaddr_in clientAddr;
char msgBuf[256];


void printHelp (char softName[]);

int main (int argc, char *argv[])
{

   int port = DEFAULT_PORT;
   char ipAddr[] = DEFAULT_IP;

   system ("clear");

   char opt;

   while ((opt = getopt(argc, argv, "i:p:h")) != -1)     //checking argumants
   {
     switch (opt)
     {
        case 'i': strcpy(ipAddr,optarg);
        break;

        case 'p': port = atoi(optarg);
        break;

        case 'h' : { printHelp(argv[0]); return 0;}
     }
  }

   sock = socket(AF_INET, SOCK_DGRAM, 0);
   if (sock < 0)
   {
      perror("Socket ERROR ");
      return SOCKET_ERROR;
   }

   servAddr.sin_family = AF_INET;
   servAddr.sin_port = htons(port);
   servAddr.sin_addr.s_addr = inet_addr(ipAddr);

   if (bind (sock,(struct sockaddr*)&servAddr,sizeof(servAddr)) < 0)  //binding socket
   {
      perror("Bind Error.");
      return BIND_ERROR;
   }

   unsigned int clientAddrSize = sizeof(struct sockaddr_in);

   while (1)
   {
         if (recvfrom(sock,msgBuf,sizeof(msgBuf),0,(struct sockaddr*)&clientAddr,&clientAddrSize) > 0)
         {
            printf("%s\n", msgBuf );
            const time_t timer = time(NULL);
            strcpy(msgBuf,ctime(&timer));
            sendto(sock,&msgBuf,sizeof(msgBuf),0,(struct sockaddr*)&clientAddr,clientAddrSize);
         }
   }

   return 0;
}

void printHelp (char *softName)
{
   printf("Simple UDP/IP server \n Usage: %s -p [server port]  - ip [server ip] \nRuning without parameters will use default ip and port (%s:%i)\n" ,softName,DEFAULT_IP,DEFAULT_PORT);
}
