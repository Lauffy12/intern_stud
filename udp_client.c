#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define SOCKET_ERROR -1
#define BIND_ERROR -2

#define DEFAULT_IP "127.0.0.1"
#define DEFAULT_PORT 8699

#define DEFAULT_SERV_IP "127.0.0.1"
#define DEFAULT_SERV_PORT 8698

int sock;
struct sockaddr_in servAddr;
struct sockaddr_in clientAddr;
char msgBuf[256];

void printHelp (char softName[]);

int main (int argc, char *argv[])
{

   int port = DEFAULT_PORT;
   char ipAddr[] = DEFAULT_IP;

   int servPort = DEFAULT_SERV_PORT;
   char servIp[] = DEFAULT_SERV_IP;

   char opt;

   while ((opt = getopt(argc, argv, "i:p:h")) != -1)     //checking argumants
   {
     switch (opt)
     {
        case 'i': strcpy(ipAddr,optarg);
        break;

        case 'p': servPort = atoi(optarg);
        break;

        case 'h' : { printHelp(argv[0]); return 0;}
     }
  }

   sock = socket(AF_INET, SOCK_DGRAM, 0);
   if (sock < 0)
   {
      perror("Socket ERROR ");
      return SOCKET_ERROR;
   }

   clientAddr.sin_family = AF_INET;
   clientAddr.sin_port = htons(port);
   clientAddr.sin_addr.s_addr = inet_addr(ipAddr);

   servAddr.sin_family = AF_INET;
   servAddr.sin_port = htons(servPort);
   servAddr.sin_addr.s_addr = inet_addr(servIp);

   if (bind (sock,(struct sockaddr*)&clientAddr,sizeof(clientAddr)) < 0)  //binding socket
   {
      perror("Bind Error.");
      return BIND_ERROR;
   }

   int servAddrSize = sizeof(struct sockaddr_in);

   strcpy(msgBuf,"Hello there, little server!");
   sendto(sock,&msgBuf,sizeof(msgBuf),0,(struct sockaddr*)&servAddr,servAddrSize);
   recvfrom(sock,msgBuf,sizeof(msgBuf),0,NULL,NULL);
   printf("%s\n",msgBuf);
   close(sock);

   return 0;
}

void printHelp (char *softName)
{
   printf("Simple UDP/IP client \n Usage: %s -p [server port]  - i [server ip] \nRuning without parameters will use default ip and port (%s:%i)\n" ,softName,DEFAULT_IP,DEFAULT_PORT);
}
