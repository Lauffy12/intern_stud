#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<arpa/inet.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<sys/socket.h>
#include<unistd.h>
#include<netinet/ip.h>
#include<netinet/ip_icmp.h>

#define DEFAULT_SOURCE_IP "127.0.0.1"
#define DEFAULT_DEST_IP "127.0.0.1"
#define IP_SIZE 16
#define BUFF_LEN 4096

unsigned short CheckSum (unsigned short *buf, int nwords);
void PrintHelp (char *softName);

int main (int argc, char *argv[])
{

  char sourceAddr[IP_SIZE] = DEFAULT_SOURCE_IP;
  char destAddr[IP_SIZE] = DEFAULT_DEST_IP;
  char opt;

  while ((opt = getopt(argc, argv, "s:d:h")) != -1)     //checking argumants
  {
    switch (opt)
    {
       case 's': strcpy(sourceAddr, optarg);
       break;

       case 'd': strcpy(destAddr, optarg);
       break;

       case 'h' : { PrintHelp(argv[0]); exit(0);}
    }
 }


  int sock = socket (AF_INET, SOCK_RAW, IPPROTO_ICMP);
  char buf[BUFF_LEN] = { 0 };
  int ttl = 1;

  int one = 1;
  const int *val = &one;
  if (setsockopt (sock, IPPROTO_IP, IP_HDRINCL, val, sizeof (one)) < 0)
    printf ("Cannot set HDRINCL!\n");

  struct sockaddr_in addr;
  addr.sin_port = htons (7);
  addr.sin_family = AF_INET;
  inet_pton (AF_INET, destAddr, &(addr.sin_addr));


  while (1)
    {
      struct ip *ip_hdr = (struct ip *) buf;
      ip_hdr->ip_hl = 5;
      ip_hdr->ip_v = 4;
      ip_hdr->ip_tos = 0;
      ip_hdr->ip_len = sizeof(struct ip)+sizeof(struct icmphdr);
      ip_hdr->ip_id = 16586;
      ip_hdr->ip_off = 0;
      ip_hdr->ip_ttl = ttl;
      ip_hdr->ip_p = IPPROTO_ICMP;
      inet_pton (AF_INET, sourceAddr, &(ip_hdr->ip_src));
      inet_pton (AF_INET, destAddr, &(ip_hdr->ip_dst));
      ip_hdr->ip_sum = CheckSum((unsigned short *) buf, sizeof(struct icmphdr));

      struct icmphdr *icmp_hdr = (struct icmphdr *) (buf + sizeof(struct ip));
      icmp_hdr->type = ICMP_ECHO;
      icmp_hdr->code = 0;
      icmp_hdr->checksum = 0;
      icmp_hdr->un.echo.id = 0;
      icmp_hdr->un.echo.sequence = ttl + 1;

      icmp_hdr->checksum = CheckSum((unsigned short *) (buf + sizeof(struct ip)), sizeof(struct icmphdr));

      sendto (sock, buf, sizeof(struct ip) + sizeof(struct icmphdr), 0, (struct sockaddr*)&addr, sizeof addr);
      char buff[BUFF_LEN] = { 0 };
      struct sockaddr_in addr2;
      socklen_t len = sizeof (struct sockaddr_in);
      recvfrom (sock, buff, sizeof(buff), 0, (struct sockaddr*) &addr2, &len);
      struct icmphdr *icmp_hdr2 = (struct icmphdr *) (buff + sizeof(struct ip));
      if (icmp_hdr2->type != 0)
      {
        printf ("ttl :%d Address: %s\n", ttl, inet_ntoa (addr2.sin_addr));
      }
      else
      {
        printf ("Reached destination: %s with ttl: %d\n", inet_ntoa (addr2.sin_addr), ttl);
        return 0;
      }
      ttl++;
    }


  return 0;
}

unsigned short CheckSum (unsigned short *buf, int nwords)
{
  unsigned long sum;
  for (sum = 0; nwords > 0; nwords--)
    sum += *buf++;
  sum = (sum >> 16) + (sum & 0xffff);
  sum += (sum >> 16);
  return ~sum;
}

void PrintHelp (char *softName)
{
   printf("traceroute using ICMP header \nUsage %s -d[destination ip address] -s[source ip address] \nDefault parameters source: %s , destination: %s" ,softName, DEFAULT_SOURCE_IP, DEFAULT_DEST_IP);
}
