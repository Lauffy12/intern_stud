all: clr hello tcp_server tcp_client udp_server udp_client tcp_fork tcp-udp_server udp_client_raw trace

hello : helloWorld.c
	gcc helloWorld.c -o hw

tcp_server : tcp_server.c
	gcc tcp_server.c -o tcp_server

tcp_client : tcp_client.c
	gcc tcp_client.c -o tcp_client

udp_server : udp_server.c
	gcc udp_server.c -o udp_server

udp_client : udp_client.c
	gcc udp_client.c -o udp_client

tcp_fork : tcp_server_fork.c
	gcc tcp_server_fork.c -o tcp_fork

tcp-udp : tcp-udp_server.c
	gcc tcp-udp_server.c -o tcp-udp_server

udp_client_raw : udp_client_raw.c
	gcc udp_client_raw.c -o udp_client_raw

trace : traceroute_ICMP.c
	gcc traceroute_ICMP.c -o trace
clean :
	rm tcp_client
	rm tcp_server
	rm udp_client
	rm udp_server
	rm hw
	rm tcp_fork
	rm tcp-udp_server
	rm udp_client_raw
	rm trace

clr :
	clear
