#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define BIND_ERROR -2
#define DEFAULY_ERROR -10
#define CONNECT_ERROR -1

#define DEFAULT_SERV_PORT 8947         // server default settings
#define DEFAULT_SERV_IP "127.0.0.1"

#define DEFAULT_PORT 8949              // client default settings
#define DEFAULT_IP "127.0.0.1"

int bytsRecv;
int sock;

int i;

char msgBuf[256];

struct sockaddr_in servAddr;
struct sockaddr_in clientAddr;

void printHelp (char softName[]);

int main(int argc, char *argv[]) {

   int servPort = DEFAULT_SERV_PORT;
   int clientPort = DEFAULT_PORT;
   char servIp[] = DEFAULT_SERV_IP;
   char clientIp[] = DEFAULT_IP;

   char opt;

   while ((opt = getopt(argc, argv, "i:p:h")) != -1)
   {
      switch (opt)
      {
         case 'i': strcpy(servIp,optarg);
         break;

         case 'p': servPort = atoi(optarg);
         break;

         case 'h' : { printHelp(argv[0]); return 0;}
      }
   }

   servAddr.sin_family = AF_INET;
   servAddr.sin_port = htons(servPort);
   servAddr.sin_addr.s_addr = inet_addr(servIp);

   clientAddr.sin_family = AF_INET;
   clientAddr.sin_port = htons (clientPort);
   clientAddr.sin_addr.s_addr = inet_addr (clientIp);

   sock = socket(AF_INET, SOCK_STREAM, 0);
   if (bind(sock,(struct sockaddr*)&clientAddr,sizeof(clientAddr)) < 0)
   {
      perror("Bind ERROR");
      return BIND_ERROR;
   }
   if (connect(sock,(struct sockaddr*)&servAddr,sizeof(servAddr)) < 0 )
   {
      perror("Connection ERROR");
      return CONNECT_ERROR;
   }

   read(sock,msgBuf,sizeof(msgBuf));
   printf("%s\n", msgBuf);

   close(sock);

   return 0;
}

void printHelp (char *softName)
{
   printf("Simple TCP/IP client \n Usage: %s -p [server port]  - i [server ip] \nRuning without parameters will use default ip and port (%s:%i)\n" ,softName,DEFAULT_IP,DEFAULT_PORT);
}
