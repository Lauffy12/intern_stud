// Server source code for TCP/IP skills training

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>


#define DEFAULT_PORT 8947
#define DEFAULT_IP "127.0.0.1"

#define LISTNER_ERROR -1
#define BIND_ERROR -2
#define DEFAULY_ERROR -10

int listner, sock;
int bytsRecv;

char msgBuf[256];

struct sockaddr_in servAddr;

void printHelp (char softName[]);

int main(int argc, char *argv[])
{

   int port = DEFAULT_PORT;
   char ipAddr[] = DEFAULT_IP;

   system ("clear");

   char opt;

   while ((opt = getopt(argc, argv, "i:p:h")) != -1)     //checking argumants
   {
     switch (opt)
     {
        case 'i': strcpy(ipAddr,optarg);
        break;

        case 'p': port = atoi(optarg);
        break;

        case 'h' : { printHelp(argv[0]); return 0;}
     }
  }

  listner = socket(AF_INET, SOCK_STREAM,0);              //creating listner socket
  if (listner < 0)
  {
     perror("Listner init error.");
     return LISTNER_ERROR;
  }

  setsockopt(listner, SOL_SOCKET, SO_REUSEADDR, "1", sizeof(char));

  servAddr.sin_family = AF_INET;
  servAddr.sin_port = htons(port);
  servAddr.sin_addr.s_addr = inet_addr(ipAddr);

  if (bind (listner,(struct sockaddr*)&servAddr,sizeof(servAddr)) < 0)  //binding socket
  {
     perror("Bind Error.");
     return BIND_ERROR;
  }


  listen(listner,100);       //listning for connections
  while (1)
  {
     sock = accept (listner, NULL, NULL);    //accept connection
     if (sock != -1)
     {
        setsockopt(listner, SOL_SOCKET, SO_REUSEADDR, "1", sizeof(char));
        const time_t timer = time(NULL);
        strcpy(msgBuf,ctime(&timer));
        write(sock,&msgBuf,sizeof(msgBuf));
        close (sock);
     }
     else
     {
        perror("Socket error");
        return DEFAULY_ERROR;
     }
  }

  return 0;
}



void printHelp (char *softName)
{
   printf("Simple TCP/IP server \n Usage: %s -p [server port]  - i [server ip] \nRuning without parameters will use default ip and port (%s:%i)\n" ,softName,DEFAULT_IP,DEFAULT_PORT);
}
