#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define SOCKET_ERROR -1
#define BIND_ERROR -2

#define DEFAULT_IP "127.0.0.1"
#define DEFAULT_PORT 8699

#define DEFAULT_SERV_IP "127.0.0.1"
#define DEFAULT_SERV_PORT 8697

#define DGRAM_SIZE 4096
#define IPHDR_SIZE 32

int sock;
struct sockaddr_in servAddr;
struct sockaddr_in clientAddr;
char msgBuf[256];
char dGram[DGRAM_SIZE];
char sourceIp[IPHDR_SIZE];
char *data;
char *pseudogram;

struct pseudoHeader
{
  u_int32_t sourceAddr;
  u_int32_t destAddr;
  u_int8_t  placeHolder;
  u_int8_t  protocol;
  u_int16_t udpLenght;
};

void PrintHelp (char softName[]);
unsigned short CheckSum(unsigned short *ptr, int byts);


int main (int argc, char *argv[])
{

   int port = DEFAULT_PORT;
   char ipAddr[] = DEFAULT_IP;

   int servPort = DEFAULT_SERV_PORT;
   char servIp[] = DEFAULT_SERV_IP;

   sock = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
   if (sock < 0)
   {
      perror("Socket ERROR ");
      return SOCKET_ERROR;
   }

/*############################################## RAW ######################################*/

   memset(dGram, 0, DGRAM_SIZE);
   struct ip *ipHeader = (struct ip*)dGram;
   struct udphdr *udpHeader = (struct udphdr*)(dGram + sizeof(struct ip));
   struct pseudoHeader pseudoHdr;

   data = dGram + sizeof(struct iphdr*) + sizeof (struct udphdr);
   strcpy(data, "Hello little server!");
   strcpy(sourceIp, DEFAULT_IP);

   pseudoHdr.sourceAddr = inet_addr(ipAddr);
   pseudoHdr.destAddr = inet_addr(servIp);
   pseudoHdr.placeHolder = 0;
   pseudoHdr.protocol = IPPROTO_UDP;
   pseudoHdr.udpLenght = htons(sizeof(struct udphdr) + strlen(data));

   servAddr.sin_family = AF_INET;
   servAddr.sin_port = htons(servPort);
   servAddr.sin_addr.s_addr = inet_addr(servIp);

   ipHeader->ip_hl = 5;
   ipHeader->ip_v = 4;
   ipHeader->ip_tos = 0;
   ipHeader->ip_len = sizeof(struct iphdr) + sizeof(struct udphdr) + strlen(data);
   ipHeader->ip_id = htonl(66566);
   ipHeader->ip_off = 0;
   ipHeader->ip_ttl = 255;
   ipHeader->ip_p = IPPROTO_UDP;
   ipHeader->ip_sum = 0;
   inet_pton (AF_INET, ipAddr, &(ipHeader->ip_src));
   inet_pton (AF_INET, servIp, &(ipHeader->ip_src));
   ipHeader->ip_sum = CheckSum((unsigned short*)dGram, ipHeader->ip_len);

   udpHeader->source = htons(port);
   udpHeader->dest = htons(servPort);
   udpHeader->len = htons(8+ strlen(data));
   udpHeader->check = 0;

   int packetSize = sizeof(struct pseudoHeader) + sizeof(struct udphdr) + strlen(data);

   pseudogram = (char*)malloc(packetSize);

   memcpy(pseudogram, (char*)&pseudoHdr, sizeof(struct pseudoHeader));
   memcpy(pseudogram + sizeof(struct pseudoHeader), udpHeader, sizeof(struct udphdr) + strlen(data));

   udpHeader->check = CheckSum((unsigned short*)pseudogram,packetSize);

   if (sendto(sock,dGram,ipHeader->ip_len, 0, (struct sockaddr*)&servAddr, sizeof(servAddr))<0)
   {
     perror("Sendto error");
   }
   else
   {
     printf("%s\n","Packet Send!");
   }

   return 0;
}

void PrintHelp (char *softName)
{
   printf("Simple UDP/RAW client \n Usage: %s -p [server port]  - i [server ip] \nRuning without parameters will use default ip and port (%s:%i)\n" ,softName,DEFAULT_IP,DEFAULT_PORT);
}

unsigned short CheckSum(unsigned short *ptr, int byts)
{
  register long sum;
  unsigned short oddByte;
  register short out;

  sum = 0;
  while (byts > 1)
  {
    sum+=*ptr++;
    byts-=2;
  }

  if (byts == 1)
  {
    oddByte = 0;
    *((u_char*)&oddByte) = *(u_char*)ptr;
    sum+=oddByte;
  }

  sum = (sum>>16) + (sum & 0xffff);
  sum = sum + (sum>16);
  out = (short)~sum;

  return out;
}
